<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Routes of Controllers
Route::get('/books','BooksController@index')
   ->name('books.create')    ;


   Route::get('/store', 'BooksController@store')
   ->name('books.store');


   Route::get('/category','CategoryController@index')
   ->name('category.create')    ;


   Route::get('/storec', 'CategoryController@store')
   ->name('category.store');





//Route::get('/guardar', 'BooksController@guardar');
//Route::resource('category', 'CategoryController');



//Routes of menus
Route::get('/books', function () {
    return view('books.create');
});

Route::get('/delete', function () {
    return view('books.delete');
});

Route::get('/categoryv', function () {
    return view('category.create');
});

Route::get('/deletec', function () {
    return view('category.delete');
});

Route::get('/view', function () {
    return view('books.view');
});
Route::get('/viewc', function () {
    return view('category.view');
});