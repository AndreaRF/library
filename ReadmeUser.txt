¡Hello Stranger!

For run the system is necessary the following steps:
  
  1- Config the archive env.
  The configuration of database is important for run the system, my configuration is:
  
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:idZd+ZTWqC3WcgKLBPJp9/yOvBW6geWeILNWV3loZxY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=Library_Books
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

2- Install mysql inside the project Library and create database Library_Books.

3- Run the system.
 for run the system is necessary following the command:
 php artisan serve

4- Run command migrate.
For the database is necessary to run the following command:
php artisan migrate

5-For see the system access the route in your browser:
 http://127.0.0.1:8000/


 ¡Thank you for your attention, have a good day! :D