import React, { Component } from 'react';

//import the components that we'll be creating here
import StackLayout from './layouts/StackLayout';

export class App extends Component {
    render() {
        return ( <
            StackLayout / >
        );
    }
}