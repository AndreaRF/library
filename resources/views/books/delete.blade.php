<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Library</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        body {
            background: #000;
        }
        
        #particles-js {
            width: 100%;
            height: 200px;
        }
        
        .content {
            text-align: center;
            color: #FFFFFF;
        }
        
        .title {
            font-size: 50px;
        }
        
        .links>a {
            color: #FFFFFF;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        
        .m-b-md {
            margin-bottom: 80px;
        }
        
        * {
            margin: 0px;
            padding: 0px;
        }
        
        #header {
            margin: auto;
            width: 500px;
            font-family: Arial, Helvetica, sans-serif;
        }
        
        ul,
        ol {
            list-style: none;
        }
        
        .nav>li {
            float: left;
            text-align: center;
        }
        
        .nav li a {
            background-color: #000;
            color: #fff;
            text-decoration: none;
            padding: 10px 12px;
            display: block;
        }
        
        .nav li a:hover {
            background-color: #000;
        }
        
        .nav li ul {
            display: none;
            position: center;
            min-width: 140px;
        }
        
        .nav li:hover>ul {
            display: block;
        }
        
        .nav li ul li {
            position: center;
        }
        
        .nav li ul li ul {
            right: -140px;
            top: 0px;
        }
        /* Style inputs, select elements and textareas */
        
        input[type=text],
        select,
        textarea {
            width: 100%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }
        /* Style the label to display next to the inputs */
        
        label {
            padding: 12px 12px 12px 0;
            display: inline-block;
        }
        /* Style the submit button */
        
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }
        /* Style the container */
        
        .container {
            border-radius: 5px;
            background-color: #000;
            padding: 20px;
            color: white;
        }
        /* Floating column for labels: 25% width */
        
        .col-25 {
            float: left;
            width: 25%;
            margin-top: 6px;
        }
        /* Floating column for inputs: 75% width */
        
        .col-75 {
            float: left;
            width: 75%;
            margin-top: 6px;
        }
        /* Clear floats after the columns */
        
        .row:after {
            float: left;
            content: "";
            display: table;
            clear: both;
        }
        /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
        
        @media screen and (max-width: 600px) {
            .col-25,
            .col-75,
            input[type=submit] {
                width: 100%;
                margin-top: 0;
            }
        }
    </style>
</head>

<body>
    <div id="particles-js"></div>
   
    <script type="text/javascript" src="{{ asset('js/particles.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/particulas.js') }}"></script>

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Books
            </div>
        </div>
    </div>
    <div id="header">
        <ul class="nav">
            <li><a href="/books">Return</a></li>
            </li>
            </li>
            </li>
        </ul>
    </div><br>
    <div class="container">
    <form method="post" action="">
          <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" name="share_name"/>
          </div>
          <div class="form-group">
              <label for="price">Author: </label>
              <input type="text" class="form-control" name="share_price"/>
          </div>
          <div class="form-group">
              <label for="quantity">Category:</label>
              <input type="text" class="form-control" name="share_qty"/>
          </div>
          <div class="form-group">
              <label for="quantity">Publish date:</label>
              <input type="text" class="form-control" name="share_qty"/>
          </div>
          <div class="form-group">
              <label for="quantity">User:</label>
              <input type="text" class="form-control" name="share_qty"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
    </div>
</body>

</html>