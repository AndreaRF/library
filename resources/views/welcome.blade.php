
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Library</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        body {
            background: #000;
        }
        
        #particles-js {
            width: 100%;
            height: 400px;
        }
        
        .content {
            text-align: center;
            color: #FFFFFF;
        }
        
        .title {
            font-size: 84px;
        }
        
        .links>a {
            color: #FFFFFF;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        
        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <div id="particles-js"></div>

    <script type="text/javascript" src="{{ asset('js/particles.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/particulas.js') }}"></script>

    <div class="flex-center position-ref full-height">

        <div class="content">
            <div class="title m-b-md">
                Welcome to the Library
            </div>

            <div class="links">
                <a href="/books">Please push for enter</a>
            </div>
        </div>
    </div>

</body>

</html>