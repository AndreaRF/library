<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Books::all();
        return view('books.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create',compact('books'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function guardar(Request $request){
      
      $data = $this->validate($request,[
        'name'=>'required',
        'author'=> 'required',
        'category' => 'required',
        'publish_date' => 'required',
        'user' => 'required'
      ]);
      $books = new Books([
        'name' => $request->get('name'),
        'author'=> $request->get('author'),
        'category'=> $request->get('category'),
        'publish_date'=> $request->get('publish_date'),
        'user'=> $request->get('user')
      ]);
      $books->save();
        return  view('books.create');


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
          $data = $this->validate($request,[
            'name'=>'required',
            'author'=> 'required',
            'category' => 'required',
            'publish_date' => 'required',
            'user' => 'required'
          ]);
          $books = new Books([
            'name' => $request->get('name'),
            'author'=> $request->get('author'),
            'category'=> $request->get('category'),
            'publish_date'=> $request->get('publish_date'),
            'user'=> $request->get('user')
          ]);
          $books->save();
            return  view('books.create');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function show(Books $books)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function edit(Books $books)
    {
        $books = books::find($books);
      return view('books.edit', compact('share'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Books $books)
    {
        $request->validate([
            'name'=>'required',
            'author'=> 'required',
            'category' => 'required',
            'published_date' => 'required',
            'user' => 'required'
          ]);
              $books = books::find($books);
              $books->name = $request->get('name');
              $books->author = $request->get('author');
              $books->category = $request->get('category');
              $books->published_date = $request->get('published_date');
              $books->user = $request->get('user');
              $books->save();
        
              return redirect('/books')->with('success', 'Stock of books has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Books  $books
     * @return \Illuminate\Http\Response
     */
    public function destroy(Books $books)
    {
        $books = books::find($books);
      $books->delete();
 
      return redirect('/books')->with('success', 'Stock has been deleted Successfully');
    }
}
